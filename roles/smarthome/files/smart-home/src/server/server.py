import time
import protocol
import websockets
import asyncio
import platform

from config import (
    SERVER_IP,
    SERVER_ID,
    SERVER_TCP_IN_PORT,
    SERVER_UDP_IN_PORT,
    SERVER_WEBSOCKET_PORT
)

from socket import socket, AF_INET, SOCK_DGRAM, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR, IPPROTO_UDP, SO_BROADCAST
from threading import Thread
from protocol.operations import HELLO, INFO, REGISTER, REGISTERED
from .client_thread import ClientThread
from logger import Logger
import device_types
from .queue_manager import QueueManager
from connection_manager import ConnectionManager


class Server:
    def __init__(self):
        self.tcp_socket = socket(AF_INET, SOCK_STREAM)
        self.tcp_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        self.tcp_socket.bind((SERVER_IP, SERVER_TCP_IN_PORT))
        Logger.log(f'TCP bind: {SERVER_IP}:{SERVER_TCP_IN_PORT}')

        self.udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
        self.udp_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        self.udp_socket.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        self.udp_socket.bind((SERVER_IP, SERVER_UDP_IN_PORT))
        Logger.log(f'UDP bind: {SERVER_IP}:{SERVER_UDP_IN_PORT}')
        self.queue_manager = QueueManager()

    def _info_handler(self):
        Logger.log('Starting UDP listener...')
        while True:
            data, addr = self.udp_socket.recvfrom(1024)
            try:
                message = protocol.decode(data)
                Logger.log(f'Received raw: {data}')
                ip = addr[0]

                if message.operation == HELLO:
                    Logger.log(f'Received HELLO from {ip}')
                    msg = protocol.Message(SERVER_ID, device_types.SERVER, SERVER_ID, INFO)
                    Logger.log(f'Sending INFO to {ip}:{message.payload["port"]}')
                    t = Thread(target=self._send, args=(ip, message.payload["port"], msg))
                    t.start()
                time.sleep(0.1)
            except protocol.ParserError:
                Logger.log(f'Parser error - data: {data}')

    def _device_listener(self):
        Logger.log('Starting TCP listener...')
        while True:
            self.tcp_socket.listen(4)
            (conn, (ip, port)) = self.tcp_socket.accept()
            _client_thread = ClientThread(ip, port, conn)
            _client_thread.start()
            time.sleep(0.1)

    async def _mobile_receiver(self, websocket, _):
        while True:
            data = await websocket.recv()
            try:
                message = protocol.decode(data)

                if message.operation == REGISTER:
                    Logger.log(f'Received REGISTER from mobile client: {message.sender_id}')
                    ConnectionManager.mobiles[message.sender_id] = websocket
                    msg = protocol.Message(SERVER_ID, device_types.SERVER, message.sender_id, REGISTERED)
                    await websocket.send(protocol.encode(msg, False))
                else:
                    self.queue_manager.queue_push(message)
            except protocol.ParserError:
                Logger.log('Websocket Parser Error')

    async def run_websockets(self):
        Logger.log('Starting Websocket server')
        Logger.log(f'Detected OS: {platform.system()}')
        if platform.system() == 'Linux':
            start_server = websockets.serve(self._mobile_receiver, '0.0.0.0', SERVER_WEBSOCKET_PORT)
        else:
            start_server = websockets.serve(self._mobile_receiver, SERVER_IP, SERVER_WEBSOCKET_PORT)

        receiver_task = asyncio.ensure_future(start_server)
        sender_task = asyncio.ensure_future(self.queue_manager.run())
        done, pending = await asyncio.wait(
            [receiver_task, sender_task],
            return_when=asyncio.FIRST_COMPLETED,
        )

    @staticmethod
    def _send(ip: str, port: int, message: protocol.Message):
        Logger.log(f'Sending data: {message.payload} to {ip}:{port}')
        send_socket = socket(AF_INET, SOCK_DGRAM)
        data = protocol.encode(message)
        addr = (ip, int(port))
        send_socket.sendto(data, addr)

    def run(self):
        t1 = Thread(target=self._info_handler, args=())
        t2 = Thread(target=self._device_listener, args=())

        t1.start()
        t2.start()

        asyncio.get_event_loop().run_until_complete(self.run_websockets())
        asyncio.get_event_loop().run_forever()

        t1.join()
        t2.join()
