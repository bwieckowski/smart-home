from threading import Thread
from socket import socket
import protocol.operations
from logger import Logger
import config
import device_types
import redis
from connection_manager import ConnectionManager
from db import DbManager


class ClientThread(Thread):

    redis_conn = redis.Redis(host=config.REDIS_IP, port=config.REDIS_PORT, db=config.REDIS_DB)
    db_manager = DbManager()

    def __init__(self, ip: str, port: int, connection: socket):
        super(ClientThread, self).__init__()
        self.ip = ip
        self.port = port
        self.connection = connection
        self.deviceId = None
        self.deviceType = None

    # TODO pop sensor thread from dict when thread die
    def register(self):
        msg = self.receive()
        if msg.operation == protocol.operations.REGISTER:
            if msg.sender_id not in ConnectionManager.sensors:
                Logger.log(f'Check sensor presence in db: {msg.sender_id}')
                resp = self.db_manager.select_sensor(msg.sender_id)
                Logger.log(f'Db response len: {len(resp["devices"])} response: {resp}')
                if len(resp['devices']) == 0:
                    Logger.log(f'Add sensor to db: {msg.sender_id}')
                    self.db_manager.insert_sensor(sensor_id=msg.sender_id, sensor_type=msg.sender_type, sensor_name=None)
                response = protocol.Message(config.SERVER_ID, device_types.SERVER, msg.sender_id, protocol.operations.REGISTERED)
                self.send(response)
                self.deviceId = msg.sender_id
                self.deviceType = msg.sender_type
                ConnectionManager.sensors[msg.sender_id] = self
            else:
                response = protocol.Message(config.SERVER_ID, device_types.SERVER, msg.sender_id, protocol.operations.NOT_REGISTERED)
                self.send(response)

    def run(self):
        self.register()
        try:
            while True:
                msg = self.receive()
                # TODO create device object?
                Logger.log(f'Pushing to queue: from {msg.sender_id}: {msg.payload}')
                self.queue_push(msg)
        except ConnectionResetError:
            Logger.log(f'{self.ip}:{self.port} disconnected from server')
            exit()

    def receive(self, block=True) -> protocol.Message:
        try:
            while block:
                data = self.connection.recv(1024)
                if data == b'':
                    raise ConnectionResetError()
                try:
                    return protocol.decode(data)
                except protocol.ParserError:
                    Logger.log(f'Parser error - data: {data}')

        except ConnectionResetError:
            Logger.log(f'{self.ip}:{self.port} out of reach')

    def send(self, msg: protocol.Message):
        try:
            self.connection.send(protocol.encode(msg))
        except ConnectionResetError:
            Logger.log(f'{self.ip}:{self.port} out of reach')

    def queue_push(self, msg: protocol.Message):
        self.redis_conn.rpush(config.REDIS_DEFAULT_QUEUE, protocol.encode(msg))
