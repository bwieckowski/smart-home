from db import DbManager


def main():
    sql_create_log_table = """ CREATE TABLE IF NOT EXISTS log (
                                            date_time text DEFAULT (DATETIME('now', '+1 hours')),
                                            log text NOT NULL
                                        ); """

    sql_create_sensors_table = """  CREATE TABLE IF NOT EXISTS sensors (
                                        id text PRIMARY KEY,
                                        type text NOT NULL,
                                        name text
                                    ); """

    sql_create_sensor_data_table = """CREATE TABLE IF NOT EXISTS sensor_data (
                                    id integer PRIMARY KEY AUTOINCREMENT,
                                    sensor_id text NOT NULL,
                                    date_time text DEFAULT (DATETIME('now', '+1 hours')),
                                    data text NOT NULL,
                                    FOREIGN KEY (sensor_id) REFERENCES sensors (id)
                                );"""

    db_manager = DbManager()

    # create tables
    db_manager.create_table(sql_create_log_table)
    db_manager.create_table(sql_create_sensors_table)
    db_manager.create_table(sql_create_sensor_data_table)


if __name__ == '__main__':
    main()
