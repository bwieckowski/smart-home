# Use in server instance - listen on all interfaces
# SERVER_IP = '0.0.0.0'
# Use in client instance - server interface ip
# SERVER_IP = '192.168.0.100'
# Use for debug
SERVER_IP = 'localhost'
SERVER_ID = 'SERVER01'
SERVER_UDP_IN_PORT = 3000
SERVER_TCP_IN_PORT = 4000
SERVER_WEBSOCKET_PORT = 8000

# REDIS
REDIS_IP = 'localhost'
REDIS_PORT = 6379
REDIS_DB = 0
REDIS_DEFAULT_QUEUE = 'queue:default'

# DB
# database = r"C:\sqlite\db\pythonsqlite.db"
SQLITE_DB_FILE = 'db_data/db'
