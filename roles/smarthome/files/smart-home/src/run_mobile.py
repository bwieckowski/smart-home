import asyncio
import client
import device_types

c = client.MobileClient('mobile01', device_types.MOBILE_CLIENT)

loop = asyncio.get_event_loop()
connection = loop.run_until_complete(c.connect())
tasks = [
        asyncio.ensure_future(c.listen(connection)),
    ]

loop.run_until_complete(asyncio.wait(tasks))
