from datetime import datetime
from db import DbManager


class Logger:
    db_manager = DbManager()
    @staticmethod
    def log(msg: str):
        time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(f'{time} | {msg}')
        Logger.db_manager.insert_log(msg)
