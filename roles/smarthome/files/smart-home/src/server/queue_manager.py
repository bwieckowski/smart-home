import time
from server import client_thread
import protocol
import protocol.operations
from logger import Logger
import redis
import config
import device_types
from connection_manager import ConnectionManager
import asyncio
from db import DbManager


class QueueManager:
    def __init__(self):
        self.redis_conn = redis.Redis(host=config.REDIS_IP, port=config.REDIS_PORT, db=config.REDIS_DB)
        self.db_manager = DbManager()

    async def run(self):
        Logger.log("Starting Queue Manager")

        while True:
            msg = self.queue_pop_message()
            if msg:
                Logger.log(f'Queue Manager handling: RECEIVER: {msg.receiver_id} | SENDER: {msg.sender_id} --> {msg.payload}')
                if msg.sender_type == device_types.MOBILE_CLIENT:
                    try:
                        if msg.receiver_id == config.SERVER_ID:
                            if msg.operation == protocol.operations.COMMAND:
                                resp_msg = None
                                if msg.payload.get("command") == 'getSensor':
                                    sensor_id = msg.payload.get("deviceId")
                                    sensor_type = msg.payload.get("deviceType")
                                    db_resp = self.db_manager.select_sensor(sensor_id, sensor_type)
                                    resp_msg = protocol.Message(sender_id=config.SERVER_ID,
                                                                sender_type=device_types.SERVER,
                                                                receiver_id=msg.sender_id,
                                                                operation=protocol.operations.RESPONSE,
                                                                payload=db_resp)
                                if msg.payload.get("command") == 'getSensorData':
                                    sensor_id = msg.payload.get("deviceId")
                                    limit = msg.payload.get("limit")
                                    db_resp = self.db_manager.select_sensor_data(sensor_id, limit)
                                    resp_msg = protocol.Message(sender_id=config.SERVER_ID,
                                                                sender_type=device_types.SERVER,
                                                                receiver_id=msg.sender_id,
                                                                operation=protocol.operations.RESPONSE,
                                                                payload=db_resp)
                                if resp_msg is not None:
                                    Logger.log(f'Queue Manager handling: RECEIVER: {resp_msg.receiver_id} | SENDER: {resp_msg.sender_id} --> {resp_msg.payload}')
                                    await ConnectionManager.mobiles.get(msg.sender_id).send(protocol.encode(resp_msg, False))
                        else:
                            client = ConnectionManager.sensors.get(msg.receiver_id)
                            client.send(msg)
                            Logger.log(f'{msg.sender_id}: sending message to {msg.receiver_id}')
                    except Exception as e:
                        print(e)
                else:
                    if msg.operation == protocol.operations.SENSOR_DATA:
                        Logger.log(f'Insert sensor_data to DB: sensorId={msg.sender_id}, sensorData={msg.payload}')
                        self.db_manager.insert_sensor_data(sensor_id=msg.sender_id, sensor_data=str(msg.payload))
                        if msg.sender_type == device_types.LIGHT_SENSOR:
                            for sensor_id, clientThread in ConnectionManager.sensors.items():
                                if clientThread.deviceType == device_types.SWITCH:
                                    msg_for_socket = protocol.Message(
                                        clientThread.deviceId,
                                        clientThread.deviceType,
                                        config.SERVER_ID,
                                        protocol.operations.COMMAND
                                    )
                                    if msg.payload.get("value") > 50:
                                        msg_for_socket.payload = {
                                            'command': 'setValue',
                                            'value': 0
                                        }
                                    else:
                                        msg_for_socket.payload = {
                                            'command': 'setValue',
                                            'value': 1
                                        }
                                    clientThread.send(msg_for_socket)
                    Logger.log('Sending message to all mobiles')
                    for mobile_id, websocket in ConnectionManager.mobiles.items():
                        try:
                            await websocket.send(protocol.encode(msg, False))
                            Logger.log(f'Message sent to {mobile_id}')
                        except:
                            ConnectionManager.mobiles.pop(mobile_id)

            await asyncio.sleep(0.1)

    def queue_pop_message(self) -> (protocol.Message, None):
        msg = self.redis_conn.rpop(config.REDIS_DEFAULT_QUEUE)
        # TODO
        if msg:
            try:
                return protocol.decode(msg)
            except protocol.ParserError:
                Logger.log('Queue Manager: Parser error')
        return None

    def queue_push(self, msg: protocol.Message):
        self.redis_conn.rpush(config.REDIS_DEFAULT_QUEUE, protocol.encode(msg))
