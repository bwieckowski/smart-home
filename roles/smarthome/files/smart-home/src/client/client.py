import protocol
import protocol.operations
import config
import random
import time
import device_types
import websockets
import asyncio

from config import SERVER_WEBSOCKET_PORT, SERVER_IP
from abc import ABC, abstractmethod

from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR, SO_BROADCAST, SOCK_STREAM, IPPROTO_UDP
from threading import Thread
from logger import Logger


class Client(ABC):
    def __init__(self, device_id: str, device_type: str):
        self.device_id: str = device_id
        self.device_type: str = device_type
        self.server_addr: tuple = None
        self.connection = None

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def register(self):
        pass

    def run(self):
        t1 = Thread(target=self.connect, args=())
        t1.start()
        t1.join()


class DeviceClient(Client):
    def __init__(self, device_id: str, device_type: str, port: int):
        super(DeviceClient, self).__init__(device_id, device_type)
        self.port: int = port

    def connect(self):
        self.findserver()
        self.connection = socket(AF_INET, SOCK_STREAM)
        self.connection.connect((self.server_addr[0], config.SERVER_TCP_IN_PORT))
        self.register()

        while True:
            msg = protocol.Message(
                self.device_id,
                self.device_type,
                config.SERVER_ID,
                protocol.operations.SENSOR_DATA,
                {
                    'value': random.randint(0, 100)
                }
            )
            self.connection.send(protocol.encode(msg))
            time.sleep(random.randint(5, 10))

    def register(self):
        msg = protocol.Message(self.device_id, self.device_type, config.SERVER_ID, protocol.operations.REGISTER)
        self.connection.send(protocol.encode(msg))
        response = self.receive()  # TODO ?

    def findserver(self):
        Logger.log('Searching for a server...')
        out_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)  # create UDP socket
        out_socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)  # this is a broadcast socket
        out_socket.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        in_socket = socket(AF_INET, SOCK_DGRAM)  # create UDP socket
        in_socket.bind(('', self.port))

        msg = protocol.Message(
            self.device_id,
            self.device_type,
            config.SERVER_ID,
            protocol.operations.HELLO,
            {
                'port': self.port
            }
        )
        data = protocol.encode(msg)
        Logger.log(f'Sending: {data}')
        # TODO replace config.SERVER_IP with true broadcast
        out_socket.sendto(data, (config.SERVER_IP, config.SERVER_UDP_IN_PORT))
        Logger.log('Waiting for response...')
        data, addr = in_socket.recvfrom(1024)  # wait for a packet
        response = protocol.decode(data)
        if response.sender_type == device_types.SERVER and response.sender_id == config.SERVER_ID:
            self.server_addr = addr
            Logger.log(f'New server: {self.server_addr}')
        else:
            Logger.log(f'Message received is not from server: {self.server_addr}')

    def receive(self, block=True):
        try:
            while block:
                data = self.connection.recv(1024)
                try:
                    return protocol.decode(data)
                except protocol.ParserError:
                    Logger.log(f'Parser error - data: {data}')

        except ConnectionResetError:
            Logger.log('Connection Reset Error')


class MobileClient:
    def __init__(self, device_id: str, device_type: str):
        self.device_id = device_id
        self.device_type = device_type
        self.connection = None

    async def connect(self):
        Logger.log('Connecting')
        uri = f"ws://{SERVER_IP}:{SERVER_WEBSOCKET_PORT}"
        self.connection = await websockets.client.connect(uri)
        Logger.log("Register websocket")
        msg = protocol.Message(self.device_id, self.device_type, config.SERVER_ID, protocol.operations.REGISTER)
        await self.connection.send(protocol.encode(msg, False))
        return self.connection

    async def listen(self, connection):
        while True:
            try:
                message = await connection.recv()
                print('Received message from server: ' + str(message))
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break
