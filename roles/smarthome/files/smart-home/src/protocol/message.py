class Message:
    def __init__(self, sender_id: str, sender_type: str, receiver_id: str, operation: str, payload: dict = None):
        self.sender_id: str = sender_id
        self.sender_type: str = sender_type
        self.receiver_id: str = receiver_id
        self.operation: str = operation
        self.payload: dict = payload if payload else {}
