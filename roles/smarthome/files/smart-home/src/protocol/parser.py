from .message import Message
import protocol.fields as fields
import json


class ParserError(Exception):
    pass


def encode(message: Message, bytes_encode=True) -> (bytes, str):
    result = message.payload.copy()
    result.update({
        fields.SENDER_ID: message.sender_id,
        fields.SENDER_TYPE: message.sender_type,
        fields.RECEIVER_ID: message.receiver_id,
        fields.OPERATION: message.operation
    })

    str_result = json.dumps(result)
    return str_result.encode() if bytes_encode else str_result


def decode(raw_data: (bytes, str)) -> Message:
    try:
        raw_data = raw_data.decode()
    except AttributeError:
        pass

    try:
        payload = json.loads(raw_data)
        return Message(
            payload.pop(fields.SENDER_ID),
            payload.pop(fields.SENDER_TYPE),
            payload.pop(fields.RECEIVER_ID),
            payload.pop(fields.OPERATION),
            payload
        )

    except (json.JSONDecodeError, KeyError):
        raise ParserError
