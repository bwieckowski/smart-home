import sqlite3
import config
import string


class DbManager:
    def __init__(self):
        self.db_name = config.SQLITE_DB_FILE

    def create_connection(self):
        try:
            return sqlite3.connect(self.db_name)
        except sqlite3.Error as e:
            print(e)

    def create_table(self, create_table_sql: str):
        try:
            c = self.create_connection().cursor()
            c.execute(create_table_sql)
        except sqlite3.Error as e:
            print(e)

    # TODO catch all exceptions + check if conn is alive
    def insert(self, sql: str, sql_vars: tuple):
        try:
            conn = self.create_connection()
            c = conn.cursor()
            c.execute(sql, sql_vars)
            conn.commit()
            return c.lastrowid
        except sqlite3.Error as e:
            print(e)

    def insert_log(self, msg: str):
        self.insert(''' INSERT INTO log(log) VALUES (?)''', (msg,))

    def insert_sensor(self, sensor_id: str, sensor_type: str, sensor_name: str = None):
        self.insert(''' INSERT INTO sensors(id,type,name) VALUES (?,?,?)''', (sensor_id, sensor_type, sensor_name))

    def insert_sensor_data(self, sensor_id: str, sensor_data: str):
        self.insert(''' INSERT INTO sensor_data(sensor_id,data) VALUES (?,?)''', (sensor_id, sensor_data))

    def select(self, sql: str, sql_vars: tuple = None):
        try:
            c = self.create_connection().cursor()
            if sql_vars is not None:
                c.execute(sql, sql_vars)
            else:
                c.execute(sql)
            resp = c.fetchall()
            if resp is None:
                return list()
            else:
                return resp
        except sqlite3.Error as e:
            print(e)

    def select_sensor(self, sensor_id: str = None, sensor_type: str = None):
        if sensor_id is not None and sensor_type is not None:
            records = self.select("SELECT * FROM sensors WHERE id=? AND type=?", (sensor_id, sensor_type))
        elif sensor_type is not None:
            records = self.select("SELECT * FROM sensors WHERE type=?", (sensor_type,))
        elif sensor_id is not None:
            records = self.select("SELECT * FROM sensors WHERE id=?", (sensor_id,))
        else:
            records = self.select("SELECT * FROM sensors")

        resp = list()
        sensors = dict()
        for row in records:
            sensor = {
                "deviceId": row[0],
                "senderType": row[1]
            }
            resp.append(sensor)
        sensors["devices"] = resp
        return sensors

    def select_sensor_data(self, sensor_id: str = None, limit: int = None):
        if sensor_id is not None and limit is not None:
            records = self.select("SELECT * FROM sensor_data WHERE sensor_id=? ORDER BY id DESC LIMIT ?", (sensor_id, limit))
        elif sensor_id is not None:
            records = self.select("SELECT * FROM sensor_data WHERE sensor_id=? ORDER BY id DESC", (sensor_id,))
        elif limit is not None:
            records = self.select("SELECT * FROM sensor_data ORDER BY id DESC LIMIT ?", (limit,))
        else:
            records = self.select("SELECT * FROM sensor_data ORDER BY id DESC")

        resp = list()
        sensor_data = dict()

        for row in records:
            sensor_dict = {
                "deviceId": row[1],
                "timestamp": row[2],
                "sensorData": row[3].strip(string.punctuation).split(": ")[1]
            }
            resp.append(sensor_dict)
        sensor_data["data"] = resp
        return sensor_data
